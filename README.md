ALS Bitbucket Challenge !!
==========================

いま流行りのBitbucket Challengeです。

指名された人※ は24時間以内に、関連団体に寄付するか、（ALSについての記述に対し）Pull Requestを送るか、またはその両方を実施してください。

※ 指名された気になって勝手に実施しても構いません
