ALS
===

Amyotrophic lateral sclerosis (ALS)—also referred to as motor neurone disease (MND), Charcot disease, and, in the United States, Lou Gehrig's disease—is a neurodegenerative disease with various causes. It is characterised by muscle spasticity, rapidly progressive weakness due to muscle atrophy, difficulty in speaking (dysarthria), swallowing (dysphagia), and breathing (dyspnea). ALS is the most common of the five motor neuron diseases.

by [Amyotrophic lateral sclerosis - Wikipedia](http://en.wikipedia.org/wiki/Amyotrophic_lateral_sclerosis)
